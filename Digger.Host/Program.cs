﻿using System;
using System.Threading.Tasks;
using Digger.Core;
using Digger.Core.Interfaces;
using Digger.Game;
using Digger.Game.Interfaces;
using Digger.Messaging.Interfaces;
using JetBrains.Annotations;
using Ninject;
using Serilog;

namespace Digger.Host
{
    [UsedImplicitly]
    class Program
    {
        static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
        }

        private static async Task MainAsync()
        {
            var kernel = BuildKernel();

            var actionQueue = kernel.Get<IActionQueue>();

            actionQueue.Initialize();
            await actionQueue.Loop();
        }

        private static IKernel BuildKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IMessanger>().To<Telegram>().InSingletonScope()
                .WithConstructorArgument("apiId", Configuration.ApiId)
                .WithConstructorArgument("apiHash", Configuration.ApiHash)
                .WithConstructorArgument("phoneNumber", Configuration.PhoneNumber)
                .WithConstructorArgument("reconnectionInterval", Configuration.ReconnectionInterval);

            kernel.Bind<ILogger>().ToMethod(_ =>
                new LoggerConfiguration().WriteTo.Console().MinimumLevel.Debug().WriteTo.File("log.txt").MinimumLevel
                    .Debug().CreateLogger()).InSingletonScope();

            kernel.Bind<IGameChat>().To<GameChat>().InSingletonScope()
                .WithConstructorArgument("peer", Configuration.GameChatName);

            kernel.Bind<IOwnerChat>().To<OwnerChat>().InSingletonScope()
                .WithConstructorArgument("peer", Configuration.OwnerChatName);

            kernel.Bind<ISquadChat>().To<SquadChat>().InSingletonScope()
                .WithConstructorArgument("peer", Configuration.SquadChatName);

            kernel.Bind<IGameApi>().To<GameApi>().InSingletonScope();

            kernel.Bind<Random>().ToSelf().InSingletonScope();

            kernel.Bind<IActionQueue>().To<ActionQueue>().InSingletonScope();

            return kernel;
        }
    }
}