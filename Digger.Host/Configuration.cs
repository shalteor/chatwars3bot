﻿using System;
using System.Configuration;

namespace Digger.Host
{
    public static class Configuration
    {
        public static string PhoneNumber => ConfigurationManager.AppSettings["PhoneNumber"];
        public static int ApiId => Convert.ToInt32(ConfigurationManager.AppSettings["ApiId"]);
        public static string ApiHash => ConfigurationManager.AppSettings["ApiHash"];
        public static int ReconnectionInterval => Convert.ToInt32(ConfigurationManager.AppSettings["ReconnectionInterval"]);

        public static string GameChatName => ConfigurationManager.AppSettings["GameChatName"];
        public static string OwnerChatName => ConfigurationManager.AppSettings["OwnerChatName"];
        public static string SquadChatName => ConfigurationManager.AppSettings["SquadChatName"];
    }
}