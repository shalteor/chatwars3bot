﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Digger.Messaging;
using Digger.Messaging.Interfaces;
using JetBrains.Annotations;
using Serilog;
using TeleSharp.TL;
using TeleSharp.TL.Messages;
using TLSharp.Core;

namespace Digger.Host
{
    [UsedImplicitly]
    public class Telegram : IMessanger
    {
        private readonly int _apiId;
        private readonly string _apiHash;
        private readonly string _phoneNumber;

        private readonly ILogger _logger;

        private TelegramClient _client;
        private Dictionary<string, TLAbsInputPeer> _peerCache;

        public Telegram(ILogger logger, int apiId, string apiHash, string phoneNumber, int reconnectionInterval)
        {
            _logger = logger;
            _apiId = apiId;
            _apiHash = apiHash;
            _phoneNumber = phoneNumber;
        }

        private async Task EnsureConnection()
        {
            if (_client != null) return;

            _logger.Information("Reconnecting to telegramm");

            _client = new TelegramClient(_apiId, _apiHash, new FileSessionStore());
            await _client.ConnectAsync();

            if (!_client.IsUserAuthorized())
            {
                _logger.Information("Authorizing");
                var hash = await _client.SendCodeRequestAsync(_phoneNumber);
                Console.WriteLine("Enter your login code:");
                var code = Console.ReadLine();
                await _client.MakeAuthAsync(_phoneNumber, hash, code);
            }
        }

        private void ResetConnection()
        {
            _client?.Dispose();
            _client = null;
        }

        private async Task EnsurePeerCache()
        {
            if (_peerCache != null) return;

            _peerCache = new Dictionary<string, TLAbsInputPeer>();

            var dialogs = (TLDialogs) await _client.GetUserDialogsAsync();
            dialogs.Chats.OfType<TLChannel>().ToList().ForEach(c => _peerCache.Add(c.Title, new TLInputPeerChannel
            {
                ChannelId = c.Id,
                AccessHash = c.AccessHash ?? 0
            }));

            dialogs.Users.OfType<TLUser>().Where(u => !string.IsNullOrWhiteSpace(u.Username))
                .ToList()
                .ForEach(u => _peerCache.Add(u.Username, new TLInputPeerUser
            {
                UserId = u.Id,
                AccessHash = u.AccessHash ?? 0
            }));
        }

        private void ResetPeerCache() => _peerCache = null;

        private async Task<T> RetryStrategy<T>(Func<Task<T>> func, int tries = 3)
        {
            for (var i = 0; i < tries; i++)
            {
                try
                {
                    return await func();
                }
                catch (InvalidOperationException e)
                {
                    ResetConnection();
                    ResetPeerCache();
                    _logger.Error(e, $"Try #{i} failed");
                }
                catch (IOException e)
                {
                    ResetConnection();
                    ResetPeerCache();
                    _logger.Error(e, $"Try #{i} failed");
                }
            }

            throw new Exception("Retry strategy failed");
        }

        public async Task<int> SendMessage(string peer, string message)
        {
            return await RetryStrategy(async () =>
            {
                await EnsureConnection();
                await EnsurePeerCache();

                var updates = await _client.SendMessageAsync(_peerCache[peer], message);

                switch (updates)
                {
                    case TLUpdates tlUpdates:
                        return tlUpdates.Updates.OfType<TLUpdateMessageID>().Single().Id;
                    case TLUpdateShortSentMessage tlUpdateShortSentMessage:
                        return tlUpdateShortSentMessage.Id;
                    default:
                        throw new InvalidOperationException();
                }
            });
        }

        public async Task<ICollection<Message>> GetMessages(string peer, int count = 10)
        {
            return await RetryStrategy(async () =>
            {
                await EnsureConnection();
                await EnsurePeerCache();

                var history = await _client.GetHistoryAsync(_peerCache[peer], 0, int.MaxValue, count);

                TLVector<TLAbsMessage> messages;
                TLVector<TLAbsUser> users;

                switch (history)
                {
                    case TLChannelMessages tlChannelMessages:
                        messages = tlChannelMessages.Messages;
                        users = tlChannelMessages.Users;
                        break;
                    case TLMessagesSlice tlMessagesSlice:
                        messages = tlMessagesSlice.Messages;
                        users = tlMessagesSlice.Users;
                        break;
                    default:
                        throw new InvalidOperationException();
                }

                return messages.OfType<TLMessage>().Select(m => new Message
                {
                    Id = m.Id,
                    Body = m.Message,
                    DateTime = (new DateTime(1970, 1, 1)).AddMilliseconds(m.Date),
                    From = users.OfType<TLUser>().FirstOrDefault(u => u.Id == m.FromId)?.Username,
                    To = peer
                }).ToList();
            });
        }
    }
}