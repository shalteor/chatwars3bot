﻿using System;
using System.Threading.Tasks;
using Digger.Core.Helpers;

namespace Digger.Core
{
    public abstract class Action
    {
        public virtual string Name => GetType().Name;
        public virtual ActionPriority Priority { get; }
        public DateTime DateTime { get; }

        protected Action(DateTime dateTime, ActionPriority priority)
        {
            DateTime = dateTime;
            Priority = priority;
        }

        public bool Precede(Action other, TimeSpan interval) => DateTime.Precede(other.DateTime, interval);

        public abstract Task Do(ActionContext context);
    }
}