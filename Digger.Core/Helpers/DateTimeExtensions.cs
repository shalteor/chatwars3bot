﻿using System;
using System.Linq;

namespace Digger.Core.Helpers
{
    public static class DateTimeExtensions
    {
        public static bool Precede(this DateTime dateTime, DateTime other, TimeSpan interval)
        {
            return dateTime < other && dateTime >= other.Subtract(interval);
        }

        public static bool Happened(this DateTime dateTime)
        {
            return dateTime < DateTime.Now;
        }

        public static DateTime Next(this DateTime startTime, TimeSpan[] timeSpans)
        {
            var currentDate = startTime.Date;
            DateTime? result = null;

            while (result == null)
            {
                var dates = timeSpans
                    .Select(i => currentDate.Add(i))
                    .OrderBy(t => t)
                    .Where(t => t >= startTime)
                    .ToList();

                if (dates.Any())
                {
                    result = dates.First();
                }

                currentDate = currentDate.AddDays(1);
            }

            return result.Value;
        }
    }
}