﻿using System;
using Digger.Core.Interfaces;
using Digger.Game.Interfaces;
using Serilog;

namespace Digger.Core
{
    public class ActionContext
    {
        public IGameApi Game { get; set; }
        public IOwnerChat Owner { get; set; }
        public ISquadChat Squad { get; set; }

        public ILogger Logger { get; set; }
        public Random Random { get; set; }
        public IActionQueue ActionQueue { get; set; }
    }
}