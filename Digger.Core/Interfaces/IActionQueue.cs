﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digger.Core.Interfaces
{
    public interface IActionQueue
    {
        IEnumerable<Action> Queue { get; }
        IEnumerable<Action> RankedQueue { get; }
        void Enqueue(Action action);
        void Initialize();
        Task Loop();
    }
}