﻿using Digger.Core.Interfaces;
using Digger.Messaging;
using Digger.Messaging.Interfaces;

namespace Digger.Core
{
    public class OwnerChat : Chat, IOwnerChat
    {
        public OwnerChat(IMessanger messanger, string peer) : base(messanger, peer)
        {
        }
    }
}