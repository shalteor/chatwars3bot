﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Digger.Core.Actions;
using Digger.Core.Interfaces;
using Digger.Game.Interfaces;
using JetBrains.Annotations;
using Serilog;

namespace Digger.Core
{
    [UsedImplicitly]
    public class ActionQueue : IActionQueue
    {
        private readonly ILogger _logger;
        private readonly IGameApi _game;
        private readonly IOwnerChat _owner;
        private readonly ISquadChat _squad;
        private readonly Random _random;

        private readonly List<Action> _queue = new List<Action>();

        private static readonly TimeSpan PriorityConcedeInterval = TimeSpan.FromMinutes(10);

        public IEnumerable<Action> Queue => _queue.OrderBy(a => a.DateTime);

        public IEnumerable<Action> RankedQueue => _queue.Where(a =>
                _queue.Where(other => other.Priority > a.Priority)
                    .All(other => !a.Precede(other, PriorityConcedeInterval)))
            .OrderBy(a => a.DateTime);

        private ActionContext ActionContext => new ActionContext
        {
            Logger = _logger,
            Random = _random,
            ActionQueue = this,
            Game = _game,
            Owner = _owner,
            Squad = _squad
        };

        public ActionQueue(ILogger logger, IGameApi game, IOwnerChat owner, ISquadChat squad, Random random)
        {
            _game = game;
            _logger = logger;
            _owner = owner;
            _squad = squad;
            _random = random;
        }

        public void Enqueue(Action action)
        {
            _logger.Debug($"{action.Name} is scheduled on {action.DateTime.ToShortTimeString()}");
            _queue.Add(action);
        }

        public void Initialize()
        {
            Enqueue(new CaravanWatcher(DateTime.Now));
            Enqueue(new IhorWatcher(DateTime.Now));
            Enqueue(new ApiWatcher(DateTime.Now));
            Enqueue(new PlayerActivityIteration(DateTime.Now));
            //Enqueue(new IhorIteration(DateTime.Now.Next(_game.IhorTimes)));
            Enqueue(new SaltAction(SaltAction.NextIteration(_game.BattleTimes)));
            Enqueue(new BattleAction(BattleAction.NextIteration(_game.BattleTimes)));

            //Enqueue(new EnergyFarmStrategy(DateTime.Now));
        }

        [SuppressMessage("ReSharper", "FunctionNeverReturns")]
        public async Task Loop()
        {
            while (true)
            {
                var action = RankedQueue.FirstOrDefault(a => a.DateTime < DateTime.Now);

                if (action != null)
                {
                    _queue.Remove(action);
                    _logger.Debug($"Running action: {action.Name}");

                    try
                    {
                        await action.Do(ActionContext);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e, $"Error runnigng action {action.Name}");
                    }
                }
                else
                {
                    await Task.Delay(1000);
                }
            }
        }
    }
}