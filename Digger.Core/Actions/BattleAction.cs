﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Digger.Core.Helpers;
using Digger.Game;

namespace Digger.Core.Actions
{
    public class BattleAction : Action
    {
        public BattleAction(DateTime dateTime) : base(dateTime, ActionPriority.Hight)
        {
        }

        public static DateTime NextIteration(TimeSpan[] battleTimes)
        {
            var firstIterations = battleTimes.Select(t => t.Subtract(TimeSpan.FromSeconds(20)));
            var secondIterations = battleTimes.Select(t => t.Subtract(TimeSpan.FromSeconds(40)));
            return DateTime.Now.Next(firstIterations.Concat(secondIterations).ToArray());
        }

        public override async Task Do(ActionContext context)
        {
            try
            {
                var messages = await context.Squad.GetMessages();
                var orders = messages.Select(m => GameApi.TryParseOrder(m.Body))
                    .Where(o => o != null)
                    .Select(o => o.Value)
                    .ToList();

                if (orders.Any())
                {
                    await context.Game.AttackCastle(orders.First());
                }
                else
                {
                    await context.Game.DefendCastle();
                }
            }
            finally
            {
                context.ActionQueue.Enqueue(new BattleAction(NextIteration(context.Game.BattleTimes)));
            }
        }
    }
}