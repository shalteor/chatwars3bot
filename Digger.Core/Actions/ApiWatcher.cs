﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class ApiWatcher : Action
    {
        private const string Version = "7/12/2018.6";

        public ApiWatcher(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        private static string FormatAction(Action action) => $"[{action.DateTime.ToShortTimeString()}] {action.Name} ({action.Priority.ToString()})";

        public override async Task Do(ActionContext context)
        {
            try
            {
                var command = (await context.Owner.GetMessages()).First().Body;

                switch (command)
                {
                    case "h":
                        await context.Owner.SendMessage("Help here..");
                        break;
                    case "aq":
                        var actionRecords = context.ActionQueue.Queue.Select(FormatAction).ToList();
                        await context.Owner.SendMessage(string.Join(Environment.NewLine, actionRecords));
                        break;
                    case "rq":
                        var rankedActionRecords = context.ActionQueue.RankedQueue.Select(FormatAction).ToList();
                        await context.Owner.SendMessage(string.Join(Environment.NewLine, rankedActionRecords));
                        break;
                    case "v":
                        await context.Owner.SendMessage(Version);
                        break;
                    case "stop":
                        //context.ActionQueue
                        break;
                }
            }
            finally 
            {
                context.ActionQueue.Enqueue(new ApiWatcher(DateTime.Now.AddSeconds(30)));
            }
        }
    }
}