﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Digger.Core.Helpers;

namespace Digger.Core.Actions
{
    public class SaltAction : Action
    {
        public SaltAction(DateTime dateTime) : base(dateTime, ActionPriority.Hight)
        {
        }

        public static DateTime NextIteration(IEnumerable<TimeSpan> battleTimes) =>
            DateTime.Now.Next(battleTimes.Select(t => t.Subtract(TimeSpan.FromMinutes(7))).ToArray());

        public override async Task Do(ActionContext context)
        {
            try
            {
                var hero = await context.Game.GetHero();

                if (hero.Money >= 250)
                {
                    await context.Game.Buy("a03");
                }
                else if (hero.Money >= 103)
                {
                    await context.Game.Buy("a23");
                }
            }
            finally
            {
                context.ActionQueue.Enqueue(new SaltAction(NextIteration(context.Game.BattleTimes)));
            }
        }
    }
}