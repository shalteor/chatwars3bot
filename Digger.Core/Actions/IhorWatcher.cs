﻿using System;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class IhorWatcher : Action
    {
        public IhorWatcher(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        public override async Task Do(ActionContext context)
        {
            try
            {
                var monster = await context.Game.CheckMonster();

                if (monster.HasValue)
                {
                    //// todo check if we have that items
                    //try
                    //{
                    //    switch (monster.Value)
                    //    {
                    //        case MonsterType.Demon:
                    //            await context.Game.Use(ItemType.HolyWater);
                    //            break;
                    //        case MonsterType.Werewolf:
                    //            await context.Game.Use(ItemType.SilverBlood);
                    //            break;
                    //        case MonsterType.Zombie:
                    //            await context.Game.Use(ItemType.AccuracyPill);
                    //            break;
                    //        case MonsterType.Vampire:
                    //            await context.Game.Use(ItemType.GarlicStew);
                    //            break;
                    //        case MonsterType.Witch:
                    //            await context.Game.Use(ItemType.VialOfDefiance);
                    //            break;
                    //        default:
                    //            throw new ArgumentOutOfRangeException();
                    //    }
                    //}
                    //catch
                    //{
                    //    // ignored
                    //}

                    await context.Game.AttackMonster();
                }
            }
            finally
            {
                var interval = DateTime.Now.AddSeconds(30);
                context.ActionQueue.Enqueue(new IhorWatcher(interval));
            }
        }
    }
}