﻿using System;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class CustomAction : Action
    {
        public override string Name { get; }

        private readonly Func<ActionContext, Task> _action;

        public CustomAction(Func<ActionContext, Task> action, DateTime dateTime, string name = "Custom action",
            ActionPriority priority = ActionPriority.Normal) : base(dateTime, priority)
        {
            Name = name;
            _action = action;
        }

        public override Task Do(ActionContext context)
        {
            return _action(context);
        }
    }
}