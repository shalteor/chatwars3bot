﻿using System;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class PlayerActivityIteration : Action
    {
        public PlayerActivityIteration(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        private static bool IsCorovanTime(DateTime time) => time.Hour >= 2 && time.Hour < 8;

        public override async Task Do(ActionContext context)
        {
            try
            {
                var hero = await context.Game.GetHero();
                var nextActionTime = DateTime.Now;

                //var nextIhorTime = DateTime.Now.Next(context.Game.IhorTimes);
                //var hoursToIhor = (int) Math.Floor((nextIhorTime - DateTime.Now).TotalHours);
                //var prepareToIhor = hoursToIhor < hero.MaxEnergy;
                //var availableEnergy = Math.Max(0, hero.CurrentEnergy + hoursToIhor - hero.MaxEnergy);
                //availableEnergy = Math.Min(hero.CurrentEnergy, availableEnergy);

                var availableEnergy = hero.CurrentEnergy;

                if (context.Game.IsArenaOpen(DateTime.Now))
                {
                    var arena = await context.Game.GetArena();

                    var availableTriesConut =
                        Math.Min(hero.Money / arena.Cost, arena.MaxBattles - arena.CurrentBattles);

                    for (var i = 0; i < availableTriesConut; i++)
                    {
                        if (!context.Game.IsArenaOpen(nextActionTime))
                        {
                            continue;
                        }

                        context.ActionQueue.Enqueue(
                            new CustomAction(c => c.Game.GoArena(), nextActionTime, "Go Arena!"));
                        nextActionTime = nextActionTime.AddSeconds(context.Random.Next(9 * 60, 11 * 60));
                    }
                }

                if (IsCorovanTime(DateTime.Now))
                {
                    for (var i = 0; i < availableEnergy / 2; i++)
                    {
                        context.ActionQueue.Enqueue(new CustomAction(c => c.Game.GoCaravan(), nextActionTime, "Go Caravan!"));
                        nextActionTime = nextActionTime.AddSeconds(context.Random.Next(9 * 60, 11 * 60));
                    }
                }
                else
                {
                    for (var i = 0; i < availableEnergy; i++)
                    {
                        switch (context.Random.Next(3))
                        {
                            case 0:
                                context.ActionQueue.Enqueue(new CustomAction(c => c.Game.GoForest(), nextActionTime, "Go Forest!"));
                                break;
                            case 1:
                                context.ActionQueue.Enqueue(new CustomAction(c => c.Game.GoSwamp(), nextActionTime, "Go Swamp!"));
                                break;
                            case 2:
                                context.ActionQueue.Enqueue(new CustomAction(c => c.Game.GoValley(), nextActionTime, "Go Valley!"));
                                break;
                        }

                        nextActionTime = nextActionTime.AddSeconds(context.Random.Next(9 * 60, 11 * 60));
                    }
                }

                //if (!prepareToIhor)
                {
                    var repeatInterval = DateTime.Now.AddMinutes(context.Random.Next(4 * 60, 5 * 60));
                    context.ActionQueue.Enqueue(new PlayerActivityIteration(repeatInterval));
                }
                //else
                //{
                //    context.ActionQueue.Enqueue(new PlayerActivityIteration(DateTime.Now.Next(context.Game.IhorTimes).AddHours(2)));
                //}
            }
            catch
            {
                var retryInterval = DateTime.Now.AddMinutes(context.Random.Next(10, 15));
                context.ActionQueue.Enqueue(new PlayerActivityIteration(retryInterval));
                throw;
            }
        }
    }
}