﻿using System;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class EnergyFarmStrategy : Action
    {
        public EnergyFarmStrategy(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        public override async Task Do(ActionContext context)
        {
            try
            {
                var hero = await context.Game.GetHero();
                var nextActionTime = DateTime.Now;

                for (var i = 0; i < hero.CurrentEnergy; i++)
                {
                    context.ActionQueue.Enqueue(new CustomAction(c => c.Game.GoForest(), nextActionTime, "Go Forest!"));
                    nextActionTime = nextActionTime.AddMinutes(context.Random.Next(8, 12));
                }

                var retryInterval = DateTime.Now.AddMinutes(context.Random.Next(3 * 60, 4 * 60));
                context.ActionQueue.Enqueue(new EnergyFarmStrategy(retryInterval));
            }
            catch
            {
                var retryInterval = DateTime.Now.AddMinutes(context.Random.Next(8, 12));
                context.ActionQueue.Enqueue(new EnergyFarmStrategy(retryInterval));
                throw;
            }
        }
    }
}