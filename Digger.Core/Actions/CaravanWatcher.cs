﻿using System;
using System.Threading.Tasks;

namespace Digger.Core.Actions
{
    public class CaravanWatcher : Action
    {
        public CaravanWatcher(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        public override async Task Do(ActionContext context)
        {
            try
            {
                if (await context.Game.CheckCaravan())
                {
                    await context.Game.DefendCaravan();
                    context.Logger.Verbose("Successfull defence");
                }
            }
            finally
            {
                context.ActionQueue.Enqueue(new CaravanWatcher(DateTime.Now.AddMinutes(context.Random.Next(1, 2))));
            }
        }
    }
}