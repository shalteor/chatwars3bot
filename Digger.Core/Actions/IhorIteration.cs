﻿using System;
using System.Threading.Tasks;
using Digger.Core.Helpers;

namespace Digger.Core.Actions
{
    public class IhorIteration : Action
    {
        public IhorIteration(DateTime dateTime) : base(dateTime, ActionPriority.Normal)
        {
        }

        public override async Task Do(ActionContext context)
        {
            try
            {
                var hero = await context.Game.GetHero();
                var nextActionTime = DateTime.Now;

                for (var i = 0; i < hero.CurrentEnergy; i++)
                {
                    context.ActionQueue.Enqueue(new CustomAction(IhorAction, nextActionTime, "Ihor action"));
                    nextActionTime = nextActionTime.AddMinutes(context.Random.Next(8, 12));
                }

                context.ActionQueue.Enqueue(new IhorIteration(DateTime.Now.Next(context.Game.IhorTimes)));
            }
            catch
            {
                var repeatInterval = DateTime.Now.AddMinutes(context.Random.Next(5, 7));
                context.ActionQueue.Enqueue(new IhorIteration(repeatInterval));
                throw;
            }
        }

        public static async Task IhorAction(ActionContext context)
        {
            //// todo check if we have it
            //try
            //{
            //    await context.Game.Use(ItemType.MonsterPheromones);
            //}
            //catch
            //{
            //    // ignored
            //}

            await context.Game.GoForest();
        }
    }
}