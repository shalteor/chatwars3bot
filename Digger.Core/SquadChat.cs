﻿using Digger.Core.Interfaces;
using Digger.Messaging;
using Digger.Messaging.Interfaces;
using JetBrains.Annotations;

namespace Digger.Core
{
    [UsedImplicitly]
    public class SquadChat : Chat, ISquadChat
    {
        public SquadChat(IMessanger messanger, string peer) : base(messanger, peer)
        {
        }
    }
}