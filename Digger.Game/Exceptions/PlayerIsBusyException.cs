﻿using System;
using System.Runtime.Serialization;

namespace Digger.Game.Exceptions
{
    public class PlayerIsBusyException : Exception
    {
        public PlayerIsBusyException()
        {
        }

        public PlayerIsBusyException(string message) : base(message)
        {
        }

        public PlayerIsBusyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PlayerIsBusyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}