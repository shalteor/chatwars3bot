﻿using System;
using System.Runtime.Serialization;

namespace Digger.Game.Exceptions
{
    public class ArenaIsClosedException : Exception
    {
        public ArenaIsClosedException()
        {
        }

        public ArenaIsClosedException(string message) : base(message)
        {
        }

        public ArenaIsClosedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ArenaIsClosedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}