﻿namespace Digger.Game.Enums
{
    public enum CastleType
    {
        Tortuga,
        Oplot,
        Amber,
        Night,
        Skala,
        Sunrise,
        Farm
    }
}