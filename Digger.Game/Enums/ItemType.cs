﻿namespace Digger.Game.Enums
{
    public enum ItemType
    {
        HolyWater,
        SilverBlood,
        AccuracyPill,
        GarlicStew,
        VialOfDefiance,

        MonsterPheromones
    }
}