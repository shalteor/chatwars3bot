﻿namespace Digger.Game.Enums
{
    public enum MonsterType
    {
        Demon,
        Werewolf,
        Zombie,
        Vampire,
        Witch
    }
}