﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Digger.Game.Dto;
using Digger.Game.Enums;
using Digger.Game.Exceptions;
using Digger.Game.Interfaces;
using JetBrains.Annotations;

namespace Digger.Game
{
    [UsedImplicitly]
    public class GameApi : IGameApi
    {
        private readonly IGameChat _game;

        public TimeSpan[] BattleTimes { get; } =
        {
            TimeSpan.FromHours(1),
            TimeSpan.FromHours(9),
            TimeSpan.FromHours(17)
        };

        public TimeSpan[] IhorTimes { get; } =
        {
            TimeSpan.FromHours(7).Add(TimeSpan.FromMinutes(4)),
            TimeSpan.FromHours(15).Add(TimeSpan.FromMinutes(5)),
            TimeSpan.FromHours(23).Add(TimeSpan.FromMinutes(6))
        };

        public GameApi(IGameChat game)
        {
            _game = game;
        }

        public async Task<HeroDto> GetHero()
        {
            var resp = await _game.SendMessageAndWaitForResponce("🏅Герой", new[] {"Экипировка"});

            var energy = Regex.Match(resp, "Выносливость: (\\d+)/(\\d+)");
            var money = Regex.Match(resp, "💰(\\d+)");

            return new HeroDto
            {
                CurrentEnergy = Convert.ToInt32(energy.Groups[1].Value),
                MaxEnergy = Convert.ToInt32(energy.Groups[2].Value),
                Money = Convert.ToInt32(money.Groups[1].Value)
            };
        }

        #region Arena

        public bool IsArenaOpen(DateTime time) => time.Hour >= 1 && time.Hour < 7 ||
                                                  time.Hour >= 9 && time.Hour < 13 ||
                                                  time.Hour >= 17 && time.Hour < 23;

        public async Task<ArenaDto> GetArena()
        {
            var resp = await _game.SendMessageAndWaitForResponce("📯Арена",
                new[] {"Добро пожаловать на арену", "Ты сейчас занят"});

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }

            var battles = Regex.Match(resp, "Поединков сегодня: (\\d+)/(\\d+)");
            var rating = Regex.Match(resp, "Твой рейтинг: (\\d+)");
            var cost = Regex.Match(resp, "Цена за вход: (\\d+)");

            return new ArenaDto()
            {
                CurrentBattles = Convert.ToInt32(battles.Groups[1].Value),
                MaxBattles = Convert.ToInt32(battles.Groups[2].Value),
                Rating = Convert.ToInt32(rating.Groups[1].Value),
                Cost = Convert.ToInt32(cost.Groups[1].Value)
            };
        }

        public async Task GoArena()
        {
            var resp = await _game.SendMessageAndWaitForResponce("▶️Быстрый бой",
                new[] {"Жажда крови одолела тебя", "Ты сейчас занят", "Арена закрыта", "У тебя нет денег"});

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }

            if (resp.Contains("Арена закрыта"))
            {
                throw new ArenaIsClosedException();
            }

            if (resp.Contains("У тебя нет денег"))
            {
                throw new NoMoneyException();
            }
        }

        #endregion

        public async Task<int> GoForest()
        {
            var resp = await _game.SendMessageAndWaitForResponce("🌲Лес",
                new[] {"Ты отправился искать", "Ты сейчас занят"});

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }

            var time = Regex.Match(resp, "Вернешься через (\\d+)").Groups[1].Value;
            return Convert.ToInt32(time) * 1000;
        }

        public async Task<int> GoSwamp()
        {
            var resp = await _game.SendMessageAndWaitForResponce("🍄Болото",
                new[] { "Приключения зовут", "Ты сейчас занят" });

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }

            var time = Regex.Match(resp, "Вернешься через (\\d+)").Groups[1].Value;
            return Convert.ToInt32(time) * 1000;
        }

        public async Task<int> GoValley()
        {
            var resp = await _game.SendMessageAndWaitForResponce("⛰️Долина",
                new[] { "Горы полны опасностей", "Ты сейчас занят" });

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }

            var time = Regex.Match(resp, "Вернешься через (\\d+)").Groups[1].Value;
            return Convert.ToInt32(time) * 1000;
        }

        #region Caravan

        public async Task GoCaravan()
        {
            var resp = await _game.SendMessageAndWaitForResponce("🗡ГРАБИТЬ КОРОВАНЫ",
                new[] {"Ты отправился грабить", "Ты сейчас занят"});

            if (resp.Contains("Ты сейчас занят"))
            {
                throw new PlayerIsBusyException();
            }
        }

        public async Task<bool> CheckCaravan()
        {
            var messages = await _game.GetMessages();
            return messages.First().Body.Contains("/go");
        }

        public async Task DefendCaravan()
        {
            await _game.SendMessage("/go");
        }

        #endregion

        public async Task AttackCastle(CastleType castleType)
        {
            await _game.SendMessageAndWaitForResponce("⚔Атака", new[] {"Смелый вояка"});

            string command;

            switch (castleType)
            {
                case CastleType.Tortuga:
                    command = "🐢";
                    break;
                case CastleType.Oplot:
                    command = "☘️";
                    break;
                case CastleType.Amber:
                    command = "🍁";
                    break;
                case CastleType.Night:
                    command = "🦇";
                    break;
                case CastleType.Skala:
                    command = "🖤";
                    break;
                case CastleType.Sunrise:
                    command = "🌹";
                    break;
                case CastleType.Farm:
                    command = "🍆";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(castleType), castleType, null);
            }

            await _game.SendMessage(command);
        }

        public async Task DefendCastle()
        {
            await _game.SendMessage("🛡Защита");
        }

        public static CastleType? TryParseOrder(string order)
        {
            if (order.Contains("🐢")) return CastleType.Tortuga;
            if (order.Contains("☘️")) return CastleType.Oplot;
            if (order.Contains("🍁")) return CastleType.Amber;
            if (order.Contains("🦇")) return CastleType.Night;
            if (order.Contains("🖤")) return CastleType.Skala;
            if (order.Contains("🌹")) return CastleType.Sunrise;
            if (order.Contains("🍆")) return CastleType.Farm;
            return null;
        }

        public async Task Buy(string itemCode)
        {
            await _game.SendMessageAndWaitForResponce($"/buy_{itemCode}", new[] {"Предмет добавлен в рюкзак"});
        }

        #region Monsters event

        public async Task<MonsterType?> CheckMonster()
        {
            var message = (await _game.GetMessages()).First();

            if (message.Body.Contains("/engage"))
            {
                return Enum.GetValues(typeof(MonsterType))
                    .OfType<MonsterType>()
                    // ReSharper disable once AssignNullToNotNullAttribute
                    .FirstOrDefault(t => message.Body.Contains(Enum.GetName(typeof(MonsterType), t)));
            }

            return null;
        }

        public async Task AttackMonster()
        {
            await _game.SendMessage("/engage");
        }

        #endregion

        public async Task Use(ItemType item)
        {
            switch (item)
            {
                case ItemType.HolyWater:
                    await _game.SendMessageAndWaitForResponce("/use_phw", new[] { "Item used" });
                    break;
                case ItemType.SilverBlood:
                    await _game.SendMessageAndWaitForResponce("/use_psb", new[] { "Item used" });
                    break;
                case ItemType.AccuracyPill:
                    await _game.SendMessageAndWaitForResponce("/use_pap", new[] { "Item used" });
                    break;
                case ItemType.GarlicStew:
                    await _game.SendMessageAndWaitForResponce("/use_pgs", new[] { "Item used" });
                    break;
                case ItemType.VialOfDefiance:
                    await _game.SendMessageAndWaitForResponce("/use_pvd", new[] { "Item used" });
                    break;
                case ItemType.MonsterPheromones:
                    await _game.SendMessageAndWaitForResponce("/use_pmp", new[] { "Item used" });
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(item), item, null);
            }
        }
    }
}