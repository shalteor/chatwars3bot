﻿namespace Digger.Game.Dto
{
    public class ArenaDto
    {
        public int CurrentBattles { get; set; }
        public int MaxBattles { get; set; }
        public int Rating { get; set; }
        public int Cost { get; set; }
    }
}