﻿namespace Digger.Game.Dto
{
    public class HeroDto
    {
        public int MaxEnergy { get; set; }
        public int CurrentEnergy { get; set; }
        public int Money { get; set; }
    }
}