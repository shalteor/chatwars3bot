﻿using System;
using System.Threading.Tasks;
using Digger.Game.Dto;
using Digger.Game.Enums;

namespace Digger.Game.Interfaces
{
    public interface IGameApi
    {
        TimeSpan[] BattleTimes { get; }
        TimeSpan[] IhorTimes { get; }
        bool IsArenaOpen(DateTime time);

        Task<HeroDto> GetHero();
        Task<ArenaDto> GetArena();
        Task<int> GoForest();
        Task<int> GoSwamp();
        Task<int> GoValley();
        Task GoArena();
        Task GoCaravan();
        Task<bool> CheckCaravan();
        Task DefendCaravan();
        Task AttackCastle(CastleType castleType);
        Task DefendCastle();
        Task Buy(string itemCode);

        // Monster event
        Task<MonsterType?> CheckMonster();
        Task AttackMonster();

        Task Use(ItemType item);
    }
}