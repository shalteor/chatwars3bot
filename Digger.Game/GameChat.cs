﻿using Digger.Game.Interfaces;
using Digger.Messaging;
using Digger.Messaging.Interfaces;
using JetBrains.Annotations;

namespace Digger.Game
{
    [UsedImplicitly]
    public class GameChat : Chat, IGameChat
    {
        public GameChat(IMessanger messanger, string peer) : base(messanger, peer)
        {
        }
    }
}