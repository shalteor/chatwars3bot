﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digger.Messaging.Interfaces
{
    public interface IChat
    {
        Task<ICollection<Message>> GetMessages(int count = 10);
        Task<int> SendMessage(string message);
        Task<string> WaitForMessage(int fromMessageId, string[] respPatterns = null, int checkInterval = 2_000, int maxWaitTime = 10_000);
        Task<string> SendMessageAndWaitForResponce(string message, string[] respPatterns, int checkInterval = 2_000, int maxWaitTime = 10_000);
    }
}