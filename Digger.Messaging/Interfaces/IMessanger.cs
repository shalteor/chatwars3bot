﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digger.Messaging.Interfaces
{
    public interface IMessanger
    {
        Task<int> SendMessage(string peer, string message);
        Task<ICollection<Message>> GetMessages(string peer, int count = 10);
    }
}