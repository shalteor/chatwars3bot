﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Digger.Messaging.Exceptions;
using Digger.Messaging.Interfaces;

namespace Digger.Messaging
{
    public class Chat : IChat
    {
        private readonly string _peer;
        private readonly IMessanger _messanger;

        public Chat(IMessanger messanger, string peer)
        {
            _messanger = messanger;
            _peer = peer;
        }

        public async Task<ICollection<Message>> GetMessages(int count = 10) => await _messanger.GetMessages(_peer, count);

        public async Task<int> SendMessage(string message) => await _messanger.SendMessage(_peer, message);

        public async Task<string> WaitForMessage(int fromMessageId, string[] respPatterns = null, int checkInterval = 2_000, int maxWaitTime = 10_000)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds < maxWaitTime)
            {
                var messages = await GetMessages();

                var resp = messages
                    .Reverse()
                    .Where(m => m.Id > fromMessageId)
                    .FirstOrDefault(m => respPatterns?.Any(p => m.Body.Contains(p)) ?? true);

                if (resp != null)
                {
                    return resp.Body;
                }

                await Task.Delay(checkInterval);
            }

            throw new NoResponceException();
        }

        public async Task<string> SendMessageAndWaitForResponce(string message, string[] respPatterns, int checkInterval = 2_000, int maxWaitTime = 10_000)
        {
            var messageId = await SendMessage(message);
            return await WaitForMessage(messageId, respPatterns, checkInterval, maxWaitTime);
        }
    }
}