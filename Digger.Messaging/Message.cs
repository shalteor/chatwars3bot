﻿using System;

namespace Digger.Messaging
{
    public class Message
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}