﻿using System;
using System.Runtime.Serialization;

namespace Digger.Messaging.Exceptions
{
    public class NoResponceException : Exception
    {
        public NoResponceException()
        {
        }

        public NoResponceException(string message) : base(message)
        {
        }

        public NoResponceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoResponceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}